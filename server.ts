// @ts-nocheck

import dotenv from 'dotenv';
dotenv.config();

import {
  ApolloServer,
  gql,
  ServerInfo,
  PubSub,
} from 'apollo-server';

import {
  Context,
} from 'apollo-server-core';
import {DocumentNode} from 'graphql';
import * as yup from 'yup';
import aws from 'aws-sdk';

import type {CakesAttributes} from './models/Cakes';
import type {CakeCommentsAttributes} from './models/CakeComments';

aws.config.update({
  region: 'eu-west-2',
  accessKeyId: process.env.AWSAccessKeyId,
  secretAccessKey: process.env.AWSSecretKey,
});

const pubsub = new PubSub();

const CAKE_ADDED = 'cakeAdded';
const CAKE_UPDATED = 'cakeUpdated';
const CAKE_REMOVED = 'cakeRemoved';
const CAKE_COMMENT_ADDED = 'cakeCommentAdded';

interface CakeContext extends Context {
  dataSources: CakeDataLoader;
  ipAddress: string;
}

type CakeDataLoader = {
  cakesLoader: any,
  cakeCommentsLoader: any,
}

type S3FileSign = {
  name: string;
  type:string;
}

type S3FileArgs = {
  file: S3FileSign;
}

type CakeArgs = {
  cake: CakesAttributes;
}

type CakeCommentArgs = {
  comment: CakeCommentsAttributes;
}

type CakeYumFactorArgs = {
  cakeId: number,
  yumFactor: number,
}

import models from './models/index';
import DataLoader from 'dataloader';

const placeholderHelper = (
    keys: ReadonlyArray<any>,
    placeholder: String = '?',
) => {
  return keys.map(() => placeholder).join(',');
};

const dataSources: Function = (): CakeDataLoader => ({
  cakesLoader: new DataLoader(async (keys) => {
    const [result] = await models.sequelize.query(`
      select * from vCakes where id in(${placeholderHelper(keys)})
    `, {
      replacements: keys,
    });

    return keys.map((key) => {
      const record = result.find((row) => row.id === key);
      return record || new Error(`No Results for ${key}`);
    });
  }),
  cakeCommentsLoader: new DataLoader(async (keys) => {
    const [result] = await models.sequelize.query(`
      select * from CakeComments where id in(${placeholderHelper(keys)})
    `, {
      replacements: keys,
    });

    return keys.map((key) => {
      const record = result.find((row) => row.id === key);
      return record || new Error(`No Results for ${key}`);
    });
  }),
  cakeYumFactorsLoader: new DataLoader(async (keys) => {
    const [result] = await models.sequelize.query(`
      select * from CakeYumFactors where id in(${placeholderHelper(keys)})
    `, {
      replacements: keys,
    });

    return keys.map((key) => {
      const record = result.find((row) => row.id === key);
      return record || new Error(`No Results for ${key}`);
    });
  }),
});

const typeDefs: DocumentNode = gql`
  type CakeType {
    id: Int
    name: String
    comment: String
    imageUrl: String
    yumFactor: Float
    userYumFactor: Int
    comments: [CakeCommentType]
  }

  type CakeCommentType {
    id: Int
    cakeId: Int
    ipAddress: String
    comment: String
    createdAt: String
  }

  type S3SignedFileType {
    url: String,
    signedRequest: String
  }

  input CakeInput {
    id: Int
    name: String
    comment: String
    imageUrl: String
  }

  input CakeCommentInput {
    cakeId: Int
    comment: String
  }

  input S3SignFileInput {
    name: String!
    type: String!
  }

  type Query {
    cakes: [CakeType]
  }

  type Mutation {
    createCake(cake: CakeInput!): CakeType
    addCakeYumFactor(cakeId: Int!, yumFactor: Int!): CakeType
    addCakeComment(comment: CakeCommentInput!): CakeCommentType
    removeCake(cake: CakeInput!): Int
    getS3SignedUrl(file: S3SignFileInput!): S3SignedFileType
  }

  type Subscription {
    cakeAdded: CakeType
    cakeUpdated: CakeType
    cakeCommentAdded: CakeCommentType
    cakeRemoved: Int
  }
`;

const resolvers = {
  CakeType: {
    comments: async (
        cake: CakesAttributes,
        args: any,
        {dataSources}: CakeContext,
    ) => {
      const [rows] = await models.sequelize.query(`
        select id from CakeComments where cakeId = :cakeId
      `, {
        replacements: {cakeId: cake.id},
      });
      return await dataSources
          .cakeCommentsLoader
          .loadMany(rows.map(({id}: any) => id));
    },
  },
  Query: {
    cakes: async (_:any, args:any, {dataSources}: CakeContext) => {
      const [rows] = await models.sequelize.query(`
        select id from Cakes order by id desc
      `);

      return await dataSources
          .cakesLoader
          .loadMany(rows.map(({id}: any) => id));
    },
  },
  Mutation: {
    getS3SignedUrl: async (
        _:any,
        {file}: S3FileArgs) => {
      const SUPPORTED_FORMATS = [
        'image/jpg',
        'image/jpeg',
        'image/gif',
        'image/png',
      ];

      const schema = yup.object().shape({
        name: yup.string().required(),
        type: yup.string().test('type', 'Invalid file format', (value) => {
          return SUPPORTED_FORMATS.includes(value);
        }),
      });

      await schema.validate(file);

      const s3 = new aws.S3();
      const S3_BUCKET = process.env.Bucket;

      const s3Params = {
        Bucket: S3_BUCKET,
        Key: file.name,
        Expires: 500,
        ContentType: file.type,
        ACL: 'public-read',
      };

      const signedRequest = await s3.getSignedUrl('putObject', s3Params);

      return {
        signedRequest,
        url: `https://${S3_BUCKET}.s3.amazonaws.com/${file.name}`,
      };
    },
    createCake: async (_:any, {cake}: CakeArgs, {dataSources}: CakeContext) => {
      const schema = yup.object().shape({
        name: yup.string().max(30).required(),
        comment: yup.string().max(200).required(),
        imageUrl: yup.string().url().required(),
      });

      await schema.validate(cake);

      const {id} = await models.Cakes.create(cake);

      const theCake = await dataSources
          .cakesLoader
          .load(id);

      pubsub.publish(CAKE_ADDED, {[CAKE_ADDED]: theCake});
      return theCake;
    },
    addCakeYumFactor: async (
        _:any,
        {cakeId, yumFactor}: CakeYumFactorArgs,
        {dataSources, ipAddress}: CakeContext,
    ) => {
      const schema = yup.object().shape({
        cakeId: yup.number().required(),
        yumFactor: yup.number().integer().min(1).max(5).required(),
      });

      await schema.validate({cakeId, yumFactor});

      await models.CakeYumFactors.upsert({
        ipAddress,
        cakeId,
        yumFactor,
      });

      const theCake = await dataSources
          .cakesLoader
          .load(cakeId);

      pubsub.publish(CAKE_UPDATED, {[CAKE_UPDATED]: theCake});
      return theCake;
    },
    addCakeComment: async (
        _:any,
        {comment}: CakeCommentArgs,
        {dataSources, ipAddress}: CakeContext,
    ) => {
      const schema = yup.object().shape({
        cakeId: yup.number().required(),
        comment: yup.string().max(255).required(),
      });

      await schema.validate(comment);

      const {id} = await models.CakeComments.create({
        ipAddress,
        ...comment,
      });

      const theCakeComment = await dataSources
          .cakeCommentsLoader
          .load(id);

      pubsub.publish(
          CAKE_COMMENT_ADDED,
          {[CAKE_COMMENT_ADDED]: theCakeComment},
      );
      return theCakeComment;
    },
    removeCake: async (
        _:any,
        {cake}: CakeArgs,
    ) => {
      // I would typically use something like graphql-shield for this check
      const count = await models.Cakes.count({where: {id: cake.id}});

      if (count === 1) {
        await models.Cakes.destroy({where: {id: cake.id}});

        pubsub.publish(CAKE_REMOVED, {[CAKE_REMOVED]: cake.id});
        return cake.id;
      }

      throw new Error(`Cake ${cake.id} doesn't exist`);
    },
  },
  Subscription: {
    [CAKE_ADDED]: {
      subscribe: () => pubsub.asyncIterator([CAKE_ADDED]),
    },
    [CAKE_UPDATED]: {
      subscribe: () => pubsub.asyncIterator([CAKE_UPDATED]),
    },
    [CAKE_COMMENT_ADDED]: {
      subscribe: () => pubsub.asyncIterator([CAKE_COMMENT_ADDED]),
    },
    [CAKE_REMOVED]: {
      subscribe: () => pubsub.asyncIterator([CAKE_REMOVED]),
    },
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({req, connection}) => {
    const ipAddress = req?.headers['uid'];

    if (connection) {
      return connection.context;
    }

    const context = {
      ipAddress,
      dataSources: dataSources(),
    };

    return context;
  },
});

server.listen(process.env.PORT || 4040).then(({url}: ServerInfo) => {
  console.log(`🚀  Server ready at ${url}`);
});
