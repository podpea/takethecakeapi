'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.sequelize.query(`
        create table Cakes (
          id int unsigned not null auto_increment primary key,
          name varchar(30) not null,
          comment varchar(200) not null,
          imageUrl varchar(1020) not null
        )
      `, {
        transaction,
      });

      await queryInterface.sequelize.query(`
        create table CakeYumFactors (
          cakeId int unsigned not null,
          ipAddress varchar(255) not null,
          yumFactor tinyint unsigned not null default 0,
          createdAt timestamp not null default now(),
          primary key(cakeId, ipAddress),
          foreign key(cakeId) references Cakes(id) on delete cascade
        )
      `, {
        transaction,
      });

      await queryInterface.sequelize.query(`
        create table CakeComments (
          id bigint unsigned not null primary key auto_increment,
          cakeId int unsigned not null,
          ipAddress varchar(255) not null,
          comment varchar(255) not null,
          createdAt timestamp not null default now(),
          foreign key(cakeId) references Cakes(id) on delete cascade
        )
      `, {
        transaction,
      });

      transaction.commit();
    } catch (err) {
      transaction.rollback();
      throw err;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.sequelize.query(`drop table CakeComments`, {
        transaction,
      });

      await queryInterface.sequelize.query(`drop table CakeYumFactors`, {
        transaction,
      });

      await queryInterface.sequelize.query(`drop table Cakes`, {
        transaction,
      });

      transaction.commit();
    } catch (err) {
      transaction.rollback();
      throw err;
    }
  },
};
