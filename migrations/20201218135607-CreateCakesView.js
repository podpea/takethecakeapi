'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return await queryInterface.sequelize.query(`
      CREATE VIEW vCakes AS
      SELECT 
          c.*,
          SUM(IFNULL(cy.yumFactor, 0)) / COUNT(cy.ipAddress) AS yumFactor
      FROM
          Cakes AS c
              LEFT JOIN
          CakeYumFactors AS cy ON c.id = cy.cakeId
      GROUP BY c.id
    `);
  },

  down: async (queryInterface, Sequelize) => {
    return await queryInterface.sequelize.query(`
      drop view vCakes
    `);
  },
};
