import {Options, Sequelize} from 'sequelize';

import {Cakes} from './Cakes';
import {CakeComments} from './CakeComments';
import {CakeYumFactors} from './CakeYumFactors';

// Open database connection
const sequelize = new Sequelize(
    process.env.Database || '',
    process.env.DBUsername || '',
    process.env.DBPassword,
    <Options> {
      host: process.env.DBHost,
      port: process.env.DBPort,
      dialect: 'mysql',
      timestamps: false,
    },
);

// Initialize each model in the database
// This must be done before associations are made
const models:any = {Cakes, CakeYumFactors, CakeComments};
const initModels = Object.keys(models).reduce(
    (carry: object, modelName: string) => {
      return {...carry, [modelName]: models[modelName].initModel(sequelize)};
    }, {},
);

export default Object.assign(
    initModels,
    {
      sequelize,
      Sequelize,
    },
);
