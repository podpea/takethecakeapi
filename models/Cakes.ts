import Sequelize, {DataTypes, Model, Optional} from 'sequelize';

export interface CakesAttributes {
  id: number;
  name: string;
  comment: string;
  imageUrl: string;
}

export type CakesPk = 'id';
export type CakesId = Cakes[CakesPk];
export type CakesCreationAttributes = Optional<CakesAttributes, CakesPk>;

export class Cakes extends Model<CakesAttributes, CakesCreationAttributes> implements CakesAttributes {
  id!: number;
  name!: string;
  comment!: string;
  imageUrl!: string;

  static initModel(sequelize: Sequelize.Sequelize): typeof Cakes {
    Cakes.init({
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(30),
        allowNull: false,
      },
      comment: {
        type: DataTypes.STRING(200),
        allowNull: false,
      },
      imageUrl: {
        type: DataTypes.STRING(1020),
        allowNull: false,
      },
    }, {
      sequelize,
      tableName: 'Cakes',
      timestamps: false,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [
            {name: 'id'},
          ],
        },
      ],
    });
    return Cakes;
  }
}
