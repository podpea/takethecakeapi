import Sequelize, {DataTypes, Model, Optional} from 'sequelize';

export interface CakeCommentsAttributes {
  id: number;
  cakeId: number;
  ipAddress: string;
  comment: string;
}

export type CakeCommentsPk = 'cakeId' | 'ipAddress';
export type CakeCommentsId = CakeComments[CakeCommentsPk];
export type CakeCommentsCreationAttributes = Optional<CakeCommentsAttributes, CakeCommentsPk>;

export class CakeComments extends Model<CakeCommentsAttributes, CakeCommentsCreationAttributes> implements CakeCommentsAttributes {
  id!: number;
  cakeId!: number;
  ipAddress!: string;
  comment!: string;

  static initModel(sequelize: Sequelize.Sequelize): typeof CakeComments {
    CakeComments.init({
      id: {
        autoIncrement: true,
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
        primaryKey: true,
      },
      cakeId: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'Cakes',
          key: 'id',
        },
      },
      ipAddress: {
        type: DataTypes.STRING(255),
        allowNull: false,
        primaryKey: true,
      },
      comment: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
    }, {
      sequelize,
      tableName: 'CakeComments',
      timestamps: false,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [
            {name: 'cakeId'},
            {name: 'ipAddress'},
          ],
        },
      ],
    });
    return CakeComments;
  }
}
