import Sequelize, {DataTypes, Model, Optional} from 'sequelize';
import type {Cakes, CakesId} from './Cakes';

export interface CakeYumFactorsAttributes {
  cakeId: number;
  ipAddress: string;
  yumFactor: number;
}

export type CakeYumFactorsPk = 'cakeId' | 'ipAddress';
export type CakeYumFactorsId = CakeYumFactors[CakeYumFactorsPk];
export type CakeYumFactorsCreationAttributes = Optional<CakeYumFactorsAttributes, CakeYumFactorsPk>;

export class CakeYumFactors extends Model<CakeYumFactorsAttributes, CakeYumFactorsCreationAttributes> implements CakeYumFactorsAttributes {
  cakeId!: number;
  ipAddress!: string;
  yumFactor!: number;

  static initModel(sequelize: Sequelize.Sequelize): typeof CakeYumFactors {
    CakeYumFactors.init({
      cakeId: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'Cakes',
          key: 'id',
        },
      },
      ipAddress: {
        type: DataTypes.STRING(255),
        allowNull: false,
        primaryKey: true,
      },
      yumFactor: {
        type: DataTypes.TINYINT.UNSIGNED,
        allowNull: false,
        defaultValue: 0,
      },
    }, {
      sequelize,
      tableName: 'CakeYumFactors',
      timestamps: false,
      indexes: [
        {
          name: 'PRIMARY',
          unique: true,
          using: 'BTREE',
          fields: [
            {name: 'cakeId'},
            {name: 'ipAddress'},
          ],
        },
      ],
    });
    return CakeYumFactors;
  }
}
