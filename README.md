# Take the Cake API
Take the Cake API is a GraphQL API which allows users to retrieve cakes, create new cakes, rate a cake, remove a cake and use subscriptions to receive push updates. You can view the API documentation at [http://localhost:4040](http://localhost:4040) while the API is running.

## Requirements

- Docker
- node 12.13
- AWS S3 with public bucket

---

## Installation

`npm install && docker-compose up`

---

## Run the server

`npm start`

## Notes

- Ensure you have created a `.env` file in the route of the project. `.env.example` shows which variables need to be populated.
- The project requires an AWS S3 bucket to be created. This is where cake photo's are uploaded. The bucket must be public and the CORS policy updated to allow access from localhost and whichever other domains you require.