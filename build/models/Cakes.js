"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cakes = void 0;
var sequelize_1 = require("sequelize");
var Cakes = /** @class */ (function (_super) {
    __extends(Cakes, _super);
    function Cakes() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Cakes.initModel = function (sequelize) {
        Cakes.init({
            id: {
                autoIncrement: true,
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                primaryKey: true
            },
            name: {
                type: sequelize_1.DataTypes.STRING(30),
                allowNull: false
            },
            comment: {
                type: sequelize_1.DataTypes.STRING(200),
                allowNull: false
            },
            imageUrl: {
                type: sequelize_1.DataTypes.STRING(1020),
                allowNull: false
            },
            yumFactor: {
                type: sequelize_1.DataTypes.TINYINT.UNSIGNED,
                allowNull: false,
                defaultValue: 0
            }
        }, {
            sequelize: sequelize,
            tableName: 'Cakes',
            timestamps: false,
            indexes: [
                {
                    name: "PRIMARY",
                    unique: true,
                    using: "BTREE",
                    fields: [
                        { name: "id" },
                    ]
                },
            ]
        });
        return Cakes;
    };
    return Cakes;
}(sequelize_1.Model));
exports.Cakes = Cakes;
