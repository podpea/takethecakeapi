"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CakeComments = void 0;
var sequelize_1 = require("sequelize");
var CakeComments = /** @class */ (function (_super) {
    __extends(CakeComments, _super);
    function CakeComments() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CakeComments.initModel = function (sequelize) {
        CakeComments.init({
            cakeId: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'Cakes',
                    key: 'id'
                }
            },
            ipAddress: {
                type: sequelize_1.DataTypes.STRING(255),
                allowNull: false,
                primaryKey: true
            },
            comment: {
                type: sequelize_1.DataTypes.STRING(255),
                allowNull: false
            }
        }, {
            sequelize: sequelize,
            tableName: 'CakeComments',
            timestamps: true,
            indexes: [
                {
                    name: "PRIMARY",
                    unique: true,
                    using: "BTREE",
                    fields: [
                        { name: "cakeId" },
                        { name: "ipAddress" },
                    ]
                },
            ]
        });
        return CakeComments;
    };
    return CakeComments;
}(sequelize_1.Model));
exports.CakeComments = CakeComments;
