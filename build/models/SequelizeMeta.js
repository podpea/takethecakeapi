"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.SequelizeMeta = void 0;
var sequelize_1 = require("sequelize");
var SequelizeMeta = /** @class */ (function (_super) {
    __extends(SequelizeMeta, _super);
    function SequelizeMeta() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SequelizeMeta.initModel = function (sequelize) {
        SequelizeMeta.init({
            name: {
                type: sequelize_1.DataTypes.STRING(255),
                allowNull: false,
                primaryKey: true
            }
        }, {
            sequelize: sequelize,
            tableName: 'SequelizeMeta',
            timestamps: false,
            indexes: [
                {
                    name: "PRIMARY",
                    unique: true,
                    using: "BTREE",
                    fields: [
                        { name: "name" },
                    ]
                },
                {
                    name: "name",
                    unique: true,
                    using: "BTREE",
                    fields: [
                        { name: "name" },
                    ]
                },
            ]
        });
        return SequelizeMeta;
    };
    return SequelizeMeta;
}(sequelize_1.Model));
exports.SequelizeMeta = SequelizeMeta;
