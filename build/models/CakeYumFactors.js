"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CakeYumFactors = void 0;
var sequelize_1 = require("sequelize");
var CakeYumFactors = /** @class */ (function (_super) {
    __extends(CakeYumFactors, _super);
    function CakeYumFactors() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CakeYumFactors.initModel = function (sequelize) {
        CakeYumFactors.init({
            cakeId: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'Cakes',
                    key: 'id',
                },
            },
            ipAddress: {
                type: sequelize_1.DataTypes.STRING(255),
                allowNull: false,
                primaryKey: true,
            },
            yumFactor: {
                type: sequelize_1.DataTypes.TINYINT.UNSIGNED,
                allowNull: false,
                defaultValue: 0,
            },
        }, {
            sequelize: sequelize,
            tableName: 'CakeYumFactors',
            timestamps: true,
            indexes: [
                {
                    name: 'PRIMARY',
                    unique: true,
                    using: 'BTREE',
                    fields: [
                        { name: 'cakeId' },
                        { name: 'ipAddress' },
                    ],
                },
            ],
        });
        return CakeYumFactors;
    };
    return CakeYumFactors;
}(sequelize_1.Model));
exports.CakeYumFactors = CakeYumFactors;
